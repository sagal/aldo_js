import xml2js from "https://esm.sh/xml2js?pin=v55";
import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const user = Deno.env.get("USER");
const pass = Deno.env.get("PASS");


async function retrieveRemoteData() {
  let response = await axiod.post(
    `http://200.40.210.226:8080/soap/ALDO/services/sim/v1.2/CArticulosweb?wsdl`,
    `
        <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:car='http://200.40.210.226:8080/soap/ALDO/schemas/sim/v1.2/CArticulosweb'>
            <soapenv:Header/>
            <soapenv:Body>
                <car:simular>
                    <!--1 or more repetitions:-->
                    <car:CArticulosweb>
                        <car:General>
                            <car:TV_art_web>
                                <!--Optional:-->
                                <car:Fecha>2018-02-02</car:Fecha>
                            </car:TV_art_web>
                            <!--1 or more repetitions:--> 
                        </car:General>
                    </car:CArticulosweb>
                </car:simular>
            </soapenv:Body>
        </soapenv:Envelope>
      `,
    {
      headers: {
        Authorization: "Basic " + btoa(user + ":" + pass),
        "Content-Type": "text/xml;charset=UTF-8",
        "SOAPAction": "http://200.40.210.226:8080/soap/ALDO/schemas/sim/v1.2/CArticulosweb/Sim/simularRequest"
      },
      timeout: 500000,
    }
  );
  let parser = new xml2js.Parser();
  let parsedResponse = await parser.parseStringPromise(response.data);
  parsedResponse = parsedResponse["soapenv:Envelope"]["soapenv:Body"][0]["ns1:simularReturn"][0]["ns1:R_CArticulosweb"][0]["ns1:R_General"][0]["ns1:R_TV_articulo_web"]
  return parsedResponse
}

async function init() {
  let articles = await retrieveRemoteData()
  for (let article of articles) {
    try {

      let sku = article["ns1:R_CodigoDeArticulo"][0].trim()
      let name = article["ns1:R_NombreArticulo"][0]
      let description = article["ns1:R_DescripciónArtículo"][0]
      let price = Math.round(((Number(article["ns1:R_Precio"][0])) + Number.EPSILON) * 100) / 100
       
      let stock = Number(article["ns1:R_StockDisponible"][0]) > 20 ? 20 : Number(article["ns1:R_StockDisponible"][0])
      let currency = article["ns1:R_CódigoMoneda"][0] == 'UYU' ? '$' : 'U$S'

      let payload = {
        sku: sku,
        integration_id: clientIntegrationId,
        client_id: clientId,
        ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
          return {
            ecommerce_id: ecommerce_id,
            properties: [
              { "stock": stock },
              { "name": name },
              { "description": description },
              {
                "price": {
                  "value": price,
                  "currency": currency,
                }
              }
            ],
            variants: []
          }
        })
      }
      await sagalDispatch(payload)
    } catch (e) {
      console.log(`${e.message}`)
    }
  }

}

await init()