import { googleDriveAccess } from "https://bitbucket.org/sagal/gdrive_js/raw/master/google_drive_access.js";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const integratorApiUrl = Deno.env.get("INTEGRATOR_API");
const googleClientId = Deno.env.get("GOOGLE_CLIENT_ID");
const googleSecret = Deno.env.get("GOOGLE_SECRET");
const refreshToken = Deno.env.get("GOOGLE_REFRESH_TOKEN");
const imagesDirectory = Deno.env.get("IMAGES_DIRECTORY");
const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID");
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY");
const awsRegion = Deno.env.get("AWS_REGION");

const bucket = new S3Bucket({
  accessKeyID: awsKeyId,
  secretKey: awsSecretKey,
  bucket: "sagal-excel-uploads-0181239103940129",
  region: awsRegion,
});

async function init() {
  let allArticleSkus = await getExistingArticles();
  for (let articleSku of allArticleSkus) {
    if (articleSku) {
      let accessToken = await getAccessToken();
      try {
        await processAllImagesForArticle(articleSku, accessToken);
      } catch (e) {
        console.log(`Unable to process images for article: ${e.message}`);
      }
    }
  }
}

async function getExistingArticles() {
  let articleSkus = [];
  for (let ecommerceId of Object.values(clientEcommerce)) {
    let response = await fetch(
      `${integratorApiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`
    );
    let allArticleSkusAsKeys = await response.json();

    for (let articleSku of Object.keys(allArticleSkusAsKeys)) {
      if (!articleSkus.find((x) => x == articleSku)) {
        articleSkus.push(articleSku);
      }
    }
  }

  return articleSkus;
}

async function sendImagesToSagal(articleSku, images) {
  let articleImages = images;

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    options: {
      merge: false,
      partial: true
    },
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      let ecommerceProps = {
        ecommerce_id: ecommerce_id,
        options: {
          override_create: {
            send: false,
          },
        },
        properties: [
          {
            images: articleImages,
          },
        ],
        variants: [],
      };
      return ecommerceProps;
    }),
  };

  await sagalDispatch(processedArticle);
}

async function getAccessToken() {
  try {
    let accessToken = await googleDriveAccess.getGoogleDriveAccessToken(
      googleClientId,
      googleSecret,
      refreshToken
    );
    return accessToken;
  } catch (e) {
    console.log(`Authorization failed: ${e.message}`);
  }
}

async function processAllImagesForArticle(articleSku, accessToken) {
  try {
    let imagesInArticle = [];

    let allImageFileInfo = await googleDriveAccess.getAllFilesFromFolder(
      imagesDirectory,
      accessToken,
      {
        queryData: [`name contains '${articleSku}'`],
        isPagedRequest: true,
      }
    );

    let buffer = [];
    for (let imageFileInfo of allImageFileInfo) {
      try {
        let batchImageProcessing = async (imageInfoInBuffer) => {
          console.log(`Retrieving image ${imageInfoInBuffer.name}`);

          let imageData = await googleDriveAccess.getSpecificFileContentById(
            imageInfoInBuffer.id,
            accessToken
          );
          imageData = await imageData.blob();
          let imageBuffer = await imageData.arrayBuffer();
          imageData = new Uint8Array(imageBuffer);

          let imageUrl = await uploadImageAndGetURI(
            imageInfoInBuffer.name,
            imageData
          );

          if (imageUrl) {
            imagesInArticle.push(imageUrl);
          }
        };

        if (buffer.length < 2) {
          buffer.push(imageFileInfo);
        }

        if (buffer.length == 2) {
          await Promise.all(buffer.map(batchImageProcessing));
          buffer = [];
        }
      } catch (e) {
        console.log(
          `Unable to retrieve image ${imageFileInfo.name}: ${e.message}`
        );
      }
    }
    if (buffer.length) {
      let batchImageProcessing = async (imageInfoInBuffer) => {
        console.log(`Retrieving image ${imageInfoInBuffer.name}`);

        let imageData = await googleDriveAccess.getSpecificFileContentById(
          imageInfoInBuffer.id,
          accessToken
        );
        imageData = await imageData.blob();
        let imageBuffer = await imageData.arrayBuffer();
        imageData = new Uint8Array(imageBuffer);

        let imageUrl = await uploadImageAndGetURI(
          imageInfoInBuffer.name,
          imageData
        );

        if (imageUrl) {
          imagesInArticle.push(imageUrl);
        }
      };

      try {
        await Promise.all(buffer.map(batchImageProcessing));
        buffer = [];
      } catch (e) {
        console.log(`Unable to upload image batch: ${e.message}`);
      }
    }

    try {
      await sendImagesToSagal(articleSku, imagesInArticle.sort());
    } catch (e) {
      throw new Error(`Unable to send image to Sagal: ${e.message}`);
    }
  } catch (e) {
    throw new Error(`Unable to retrieve images for upload: ${e.message}`);
  }
}

async function uploadImageAndGetURI(imageName, imageData) {
  try {
    await bucket.putObject(`/aldo_imgs/${imageName.toLowerCase()}`, imageData, {
      contentType: "image/jpeg",
      acl: "public-read",
    });
    console.log(
      `Image uploaded: https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/aldo_imgs/${imageName.toLowerCase()}`
    );
    return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/aldo_imgs/${imageName.toLowerCase()}`;
  } catch (e) {
    console.log(`Unable to upload image: ${e.message}`);
  }
}

await init();
